package DAO;

import Model.Especialidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class EspecialidadeDAO {
    private Connection conn;
    private PreparedStatement pstm;
    private ResultSet rs;
 
    private final String SELECT_TUDO="SELECT id as Id, descricao as Descrição FROM especialidade";
    private final String SELECT_TUDO_MEDICO="SELECT e.id as Id, e.descricao as Descrição FROM especialidade e join medico_especialidade m on(e.id = m.id_especialidade) where m.id_medico = ?";
    private final String SELECT_TUDO_LIST="SELECT CONCAT(id, ' - ', descricao) FROM especialidade ORDER BY descricao;";
    private final String SELECT_ID="SELECT * FROM especialidade WHERE id = ?";
    private final String INSERT=" INSERT INTO especialidade (descricao)"
            + "VALUES(?)";
    private final String UPDATE=" UPDATE especialidade SET descricao = ? WHERE id = ?";
    private final String DELETE=" DELETE FROM especialidade WHERE id = ?";
    
    public LinkedList<Especialidade> selecionar(){
        LinkedList<Especialidade> especialidades = new LinkedList();
        Especialidade info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            while(rs.next()){
                info = new Especialidade();
                info.setId(rs.getInt("id"));
                info.setDescricao(rs.getString("descricao"));
                especialidades.add(info);
            }
 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return especialidades;
    }
    
        public Vector<String> selecionarListaMedicos(){
        Vector<String> especialidades = new Vector<String>();
        Especialidade info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO_LIST);
            rs=pstm.executeQuery();
            while(rs.next()){
                especialidades.add(rs.getString(1));
            }
 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return especialidades;
    }
    
    public Especialidade selecionar(int id){
        Especialidade info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Especialidade();
                info.setId(rs.getInt("id"));
                info.setDescricao(rs.getString("descricao"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }
    

    public void inserir(Especialidade info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT);
            pstm.setString(1, info.getDescricao());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void atualizar(Especialidade info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(UPDATE);
            pstm.setString(1, info.getDescricao());
            pstm.setInt(2, info.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void excluir(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    public void excluir(Especialidade l){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, l.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    public DefaultTableModel preencheJTable(int idMedico) {

        try {
            conn=Conexao.conectar();
            if (idMedico > 0){
                pstm=conn.prepareStatement(SELECT_TUDO_MEDICO);
                pstm.setInt(1, idMedico);
            } else {
                pstm=conn.prepareStatement(SELECT_TUDO);
            }            
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
            
            return new DefaultTableModel(data, columnNames) {

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
    
    public Vector<String> preencheComboBusca(){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount(); 
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }
            return columnNames;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
}
