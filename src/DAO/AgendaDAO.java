package DAO;

import Model.Agenda;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class AgendaDAO {
    private Connection conn;
    private PreparedStatement pstm;
    private ResultSet rs;
 
    private final String SELECT_TUDO="SELECT * FROM agenda";
    private final String SELECT_GRID="CALL gera_grid(?, ?)";
    private final String SELECT_ID="SELECT * FROM agenda WHERE id = ?";
    private final String SELECT_DATA_HORA="SELECT * FROM agenda WHERE id_medico = ? and data = ? and hora = ?";
    private final String SELECT_AGENDAMENTO="SELECT date_format(a.data, '%d/%m/%Y') AS data, DATE_format(a.hora, '%H:%i') AS hora, p.nome, a.descricao AS descricao FROM agenda AS a left JOIN paciente p ON(a.id_paciente = p.id) WHERE a.id_medico = ? and a.data between ? and ?";
    private final String INSERT=" INSERT INTO agenda (descricao, id_medico, id_especialidade, data, hora, observacao, id_paciente) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE=" UPDATE agenda SET descricao = ?, id_medico = ?, id_especialidade = ?, data = ?, hora = ?, observacao = ?, id_paciente = ? WHERE id = ?";
    private final String DELETE=" DELETE FROM agenda WHERE id = ?";
    
    public LinkedList<Agenda> selecionar(){
        LinkedList<Agenda> agendas = new LinkedList();
        Agenda info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            while(rs.next()){
                info = new Agenda();
                info.setId(rs.getInt("id"));
                info.setDescricao(rs.getString("descricao"));
                info.setId_medico(rs.getInt("id_medico"));
                info.setId_especialidade(rs.getInt("id_especialidade"));
                info.setData(rs.getDate("data"));
                info.setHora(rs.getTime("hora"));
                info.setObservacao(rs.getString("observacao"));
                info.setId_paciente(rs.getInt("id_paciente"));

                agendas.add(info);
            }
 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return agendas;
    }
    
    public Agenda selecionar(int id){
        Agenda info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Agenda();
                info.setId(rs.getInt("id"));
                info.setDescricao(rs.getString("descricao"));
                info.setId_medico(rs.getInt("id_medico"));
                info.setId_especialidade(rs.getInt("id_especialidade"));
                info.setData(rs.getDate("data"));
                info.setHora(rs.getTime("hora"));
                info.setObservacao(rs.getString("observacao"));
                info.setId_paciente(rs.getInt("id_paciente"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;        
    }
    
    public Agenda selecionar(String dt, String hr, int idMedico){
        Agenda info=null;
        Date data = Controller.Utils.stringToDate(dt);
        Time hora = Controller.Utils.stringToHora(hr);         
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_DATA_HORA);
            pstm.setInt(1, idMedico);
            pstm.setDate(2, data);
            pstm.setTime(3, hora);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Agenda();
                info.setId(rs.getInt("id"));
                info.setDescricao(rs.getString("descricao"));
                info.setId_medico(rs.getInt("id_medico"));
                info.setId_especialidade(rs.getInt("id_especialidade"));
                info.setData(rs.getDate("data"));
                info.setHora(rs.getTime("hora"));
                info.setObservacao(rs.getString("observacao"));
                info.setId_paciente(rs.getInt("id_paciente"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
        
    }
            
    public void inserir(Agenda info){
        int id = 0;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT);
            pstm.setString(1, info.getDescricao());
            pstm.setInt(2, info.getId_medico());
            pstm.setInt(3, info.getId_especialidade());
            pstm.setDate(4, info.getData());
            pstm.setTime(5, info.getHora());
            pstm.setString(6, info.getObservacao());
            pstm.setInt(7, info.getId_paciente());            
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
 
    public void atualizar(Agenda info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(UPDATE);
            pstm.setString(1, info.getDescricao());
            pstm.setInt(2, info.getId_medico());
            pstm.setInt(3, info.getId_especialidade());
            pstm.setDate(4, info.getData());
            pstm.setTime(5, info.getHora());
            pstm.setString(6, info.getObservacao());
            pstm.setInt(7, info.getId_paciente());   
            pstm.setInt(8, info.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void excluir(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    
    public void excluir(Agenda l){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, l.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    public DefaultTableModel preencheJTable(String d1, String d2) {
        
        Date date1 = Controller.Utils.stringToDate(d1);
        Date date2= Controller.Utils.stringToDate(d2);            
        DefaultTableModel modelo = null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_GRID);
            pstm.setDate(1, date1);
            pstm.setDate(2, date2);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
 
        modelo = new DefaultTableModel(data, columnNames)  {

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
            return modelo;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return modelo;
    }
    
    
    public void alimentaAgenda(JTable tabela, String d1, String d2, String idMedico){
        ArrayList<String[]> resultados = new ArrayList<String[]>();   
        Date date1 = Controller.Utils.stringToDate(d1);
        Date date2= Controller.Utils.stringToDate(d2);          
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_AGENDAMENTO);
            pstm.setInt(1, Integer.parseInt(idMedico));
            pstm.setDate(2, date1);
            pstm.setDate(3, date2);
            rs=pstm.executeQuery();
            while(rs.next()){
                resultados.add(new String[] {rs.getString("data"), rs.getString("hora"), rs.getString("nome"), rs.getString("descricao")});
            }
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }            

        for (int col = 1; col <= tabela.getModel().getColumnCount(); col++){
            for(String[] resultado : resultados){                        
                if (resultado[0].equals(tabela.getModel().getColumnName(col))){                        
                    for (int row = 0; row < tabela.getModel().getRowCount(); row++){
                        if (resultado[1].equals(tabela.getModel().getValueAt(row, 0))){
                            tabela.getModel().setValueAt("<html><center><b>P: </b>" + resultado[2] + "<br><b>D: </b>" + resultado[3] + "</center></html>", row, col);
                        }
                    }                    
                }
            }
        }
    }
    
    public Vector<String> preencheComboBusca(){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }
            return columnNames;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
}
