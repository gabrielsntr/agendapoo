package DAO;

import Model.Paciente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class PacienteDAO {
    private Connection conn;
    private PreparedStatement pstm;
    private ResultSet rs;
    
 
    private final String SELECT_TUDO="SELECT * FROM paciente";
    private final String SELECT_TUDO_VIEW="SELECT * FROM vw_paciente";
    private final String SELECT_ID="SELECT * FROM paciente WHERE id = ?";
    private final String INSERT="INSERT INTO paciente (nome, rg, cpf, data_nascimento, sexo, profissao, estado_civil, email, telefone, telefone_comercial, celular, peso, altura, tipo_sanguineo, is_alergico, alergico, is_doenca_cronica, doenca_cronica, is_fumante, is_cirurgia_anterior, cirurgia_anterior, observacao) "
            + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE=" UPDATE paciente SET nome = ?, rg = ?, cpf = ?, data_nascimento = ?, sexo = ?, profissao = ?, estado_civil = ?, email = ?, telefone = ?, telefone_comercial = ?, celular = ?, peso = ?, altura = ?, tipo_sanguineo = ?, is_alergico = ?, alergico = ?, is_doenca_cronica = ?, doenca_cronica = ?, is_fumante = ?, is_cirurgia_anterior = ?, cirurgia_anterior = ?, observacao = ? WHERE id = ?";
    private final String DELETE=" DELETE FROM paciente WHERE id = ?";
    
    public LinkedList<Paciente> selecionar(){
        LinkedList<Paciente> pacientes = new LinkedList();
        Paciente info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            while(rs.next()){
                info = new Paciente();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setRg(rs.getString("rg"));
                info.setCpf(rs.getString("cpf"));
                info.setDataNascimento(rs.getDate("data_nascimento"));
                info.setSexo(rs.getInt("sexo"));
                info.setProfissao(rs.getString("profissao"));
                info.setEstadoCivil(rs.getInt("estado_civil"));
                info.setEmail(rs.getString("email"));
                info.setTelefone(rs.getString("telefone"));
                info.setTelefoneComercial(rs.getString("telefone_comercial"));
                info.setCelular(rs.getString("celular"));
                info.setPeso(rs.getFloat("peso"));
                info.setAltura(rs.getFloat("altura"));
                info.setTipo_sanguineo(rs.getInt("tipo_sanguineo"));
                info.setIsAlergico(rs.getBoolean("is_alergico"));
                info.setAlergico(rs.getString("alergico"));
                info.setIsDoencaCronica(rs.getBoolean("is_doenca_cronica"));
                info.setDoencaCronica(rs.getString("doenca_cronica"));
                info.setIsFumante(rs.getBoolean("is_fumante"));
                info.setIsCirurgiaAnterior(rs.getBoolean("is_cirurgia_anterior"));
                info.setCirurgiaAnterior(rs.getString("cirurgia_anterior"));
                info.setObservacao(rs.getString("observacao"));
                pacientes.add(info);
            }
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return pacientes;
    }
    
    public Paciente selecionar(int id){
        Paciente info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Paciente();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setRg(rs.getString("rg"));
                info.setCpf(rs.getString("cpf"));
                info.setDataNascimento(rs.getDate("data_nascimento"));
                info.setSexo(rs.getInt("sexo"));
                info.setProfissao(rs.getString("profissao"));
                info.setEstadoCivil(rs.getInt("estado_civil"));
                info.setEmail(rs.getString("email"));
                info.setTelefone(rs.getString("telefone"));
                info.setTelefoneComercial(rs.getString("telefone_comercial"));
                info.setCelular(rs.getString("celular"));
                info.setPeso(rs.getFloat("peso"));
                info.setAltura(rs.getFloat("altura"));
                info.setTipo_sanguineo(rs.getInt("tipo_sanguineo"));
                info.setIsAlergico(rs.getBoolean("is_alergico"));
                info.setAlergico(rs.getString("alergico"));
                info.setIsDoencaCronica(rs.getBoolean("is_doenca_cronica"));
                info.setDoencaCronica(rs.getString("doenca_cronica"));
                info.setIsFumante(rs.getBoolean("is_fumante"));
                info.setIsCirurgiaAnterior(rs.getBoolean("is_cirurgia_anterior"));
                info.setCirurgiaAnterior(rs.getString("cirurgia_anterior"));
                info.setObservacao(rs.getString("observacao"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }
    
    public void inserir(Paciente info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT);
            pstm.setString(1, info.getNome());
            pstm.setString(2, info.getRg());
            pstm.setString(3, info.getCpf());
            pstm.setDate(4, info.getDataNascimento());
            pstm.setInt(5, info.getSexo());
            pstm.setString(6, info.getProfissao());
            pstm.setInt(7, info.getEstadoCivil());
            pstm.setString(8, info.getEmail());
            pstm.setString(9, info.getTelefone());
            pstm.setString(10, info.getTelefoneComercial());
            pstm.setString(11, info.getCelular());
            pstm.setFloat(12, info.getPeso());
            pstm.setFloat(13, info.getAltura());
            pstm.setInt(14, info.getTipo_sanguineo());
            pstm.setBoolean(15, info.isIsAlergico());
            pstm.setString(16, info.getAlergico());
            pstm.setBoolean(17, info.isIsDoencaCronica());
            pstm.setString(18, info.getDoencaCronica());
            pstm.setBoolean(19, info.isIsFumante());
            pstm.setBoolean(20, info.isIsCirurgiaAnterior());
            pstm.setString(21, info.getCirurgiaAnterior());
            pstm.setString(22, info.getObservacao());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void atualizar(Paciente info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(UPDATE);
            pstm.setString(1, info.getNome());
            pstm.setString(2, info.getRg());
            pstm.setString(3, info.getCpf());
            pstm.setDate(4, info.getDataNascimento());
            pstm.setInt(5, info.getSexo());
            pstm.setString(6, info.getProfissao());
            pstm.setInt(7, info.getEstadoCivil());
            pstm.setString(8, info.getEmail());
            pstm.setString(9, info.getTelefone());
            pstm.setString(10, info.getTelefoneComercial());
            pstm.setString(11, info.getCelular());
            pstm.setFloat(12, info.getPeso());
            pstm.setFloat(13, info.getAltura());
            pstm.setInt(14, info.getTipo_sanguineo());
            pstm.setBoolean(15, info.isIsAlergico());
            pstm.setString(16, info.getAlergico());
            pstm.setBoolean(17, info.isIsDoencaCronica());
            pstm.setString(18, info.getDoencaCronica());
            pstm.setBoolean(19, info.isIsFumante());
            pstm.setBoolean(20, info.isIsCirurgiaAnterior());
            pstm.setString(21, info.getCirurgiaAnterior());
            pstm.setString(22, info.getObservacao());
            pstm.setInt(23, info.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void excluir(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    public void excluir(Paciente l){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, l.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    public DefaultTableModel preencheJTable() {

        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO_VIEW);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
            
            return new DefaultTableModel(data, columnNames){

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
    
    public Vector<String> preencheComboBusca(){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO_VIEW);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }
            return columnNames;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
}
