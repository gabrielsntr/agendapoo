package DAO;

import Model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class UsuarioDAO {
    private Connection conn;
    private PreparedStatement pstm;
    private ResultSet rs;
 
    private final String SELECT_TUDO="SELECT id, usuario FROM usuario";
    private final String SELECT_ID="SELECT * FROM usuario WHERE id = ?";
    private final String SELECT_LOGIN="SELECT * FROM usuario WHERE usuario = ? and senha = sha1(?)";
    private final String INSERT=" INSERT INTO usuario (usuario, senha)"
            + "VALUES(?,sha1(?))";
    private final String UPDATE=" UPDATE usuario SET usuario = ?, senha = sha1(?) WHERE ID = ?";
    private final String DELETE=" DELETE FROM usuario WHERE ID = ?";
    
    public LinkedList<Usuario> selecionar(){
        LinkedList<Usuario> usuarios = new LinkedList();
        Usuario info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            while(rs.next()){
                info = new Usuario();
                info.setId(rs.getInt("id"));
                info.setUsuario(rs.getString("usuario"));
                usuarios.add(info);
            }
 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return usuarios;
    }
    
    public Usuario selecionar(int id){
        Usuario info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Usuario();
                info.setId(rs.getInt("id"));
                info.setUsuario(rs.getString("usuario"));
                info.setSenha(rs.getString("senha"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }
    
    public Usuario selecionar(String nome, String senha){
        Usuario info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_LOGIN);
            pstm.setString(1, nome);
            pstm.setString(2, senha);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Usuario();
                info.setId(rs.getInt("id"));
                info.setUsuario(rs.getString("usuario"));
                info.setSenha(rs.getString("senha"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }


    public void inserir(Usuario info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT);
            pstm.setString(1, info.getUsuario());
            pstm.setString(2, info.getSenha());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void atualizar(Usuario info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(UPDATE);
            pstm.setString(1, info.getUsuario());
            pstm.setString(2, info.getSenha());
            pstm.setInt(3, info.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void excluir(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    public void excluir(Usuario l){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, l.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
        public DefaultTableModel preencheJTable() {

        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
            
            return new DefaultTableModel(data, columnNames){

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
    
    public Vector<String> preencheComboBusca(){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }
            return columnNames;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
    
}
