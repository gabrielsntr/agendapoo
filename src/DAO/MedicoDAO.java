package DAO;

import Model.Medico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class MedicoDAO {
    private Connection conn;
    private PreparedStatement pstm;
    private ResultSet rs;
 
    private final String SELECT_TUDO="SELECT * FROM medico";
    private final String SELECT_TUDO_VIEW="SELECT * FROM vw_medicos";
    private final String SELECT_ID="SELECT * FROM medico WHERE id = ?";
    private final String SELECT__ESPECIALIDADE_ID="SELECT id_especialidade FROM medico_especialidade WHERE id_medico = ?";
    private final String INSERT=" INSERT INTO medico (nome, nome_reduzido, crm, is_ativo)"
            + "VALUES(?, ?, ?, ?);";
    private final String INSERT_ESPECIALIDADES="INSERT IGNORE INTO medico_especialidade (id_medico, id_especialidade) VALUES (?, ?)";
    private final String UPDATE=" UPDATE medico SET nome = ?, nome_reduzido = ?, crm = ?, is_ativo = ? WHERE id = ?";
    private final String DELETE=" DELETE FROM medico WHERE id = ?";
    private final String DELETE_ESPECIALIDADE=" DELETE FROM medico_especialidade WHERE id_medico = ?";
    public LinkedList<Medico> selecionar(){
        LinkedList<Medico> medicos = new LinkedList();
        Medico info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            while(rs.next()){
                info = new Medico();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setNomeReduzido(rs.getString("nome_reduzido"));
                info.setCrm(rs.getString("crm"));
                medicos.add(info);
            }
 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return medicos;
    }
    
    public Medico selecionar(int id){
        Medico info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Medico();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setNomeReduzido(rs.getString("nome_reduzido"));
                info.setCrm(rs.getString("crm"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }
    
    public int[] selecionarEspecialidade(int id){
        int[] especialidades = new int[20];        
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT__ESPECIALIDADE_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            int cont = 0;
            while(rs.next()){
                especialidades[cont] = rs.getInt(1);
                cont++;
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return especialidades;
    }
    
    public int inserir(Medico info){
        int id = 0;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, info.getNome());
            pstm.setString(2, info.getNomeReduzido());
            pstm.setString(3, info.getCrm());
            pstm.setBoolean(4, info.isIsAtivo());
            int num = pstm.executeUpdate();
            rs = pstm.getGeneratedKeys();
            while(rs.next()){
                id = rs.getInt(1);
                System.out.println(id);
            }
            return id;
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return id;
    }
    
    public void inserirEspecialidades(int id_medico, int id_especialidade){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT_ESPECIALIDADES);
            pstm.setInt(1, id_medico);
            pstm.setInt(2, id_especialidade);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
 
    public void atualizar(Medico info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(UPDATE);
            pstm.setString(1, info.getNome());
            pstm.setString(2, info.getNomeReduzido());
            pstm.setString(3, info.getCrm());
            pstm.setBoolean(4, info.isIsAtivo());
            pstm.setInt(5, info.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void excluir(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
        public void excluirEspecialidade(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE_ESPECIALIDADE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    public void excluir(Medico l){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, l.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    public DefaultTableModel preencheJTable() {

        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO_VIEW);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
            
            return new DefaultTableModel(data, columnNames){

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
    
    public Vector<String> preencheComboBusca(){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO_VIEW);
            rs=pstm.executeQuery();
            
            ResultSetMetaData metaData = rs.getMetaData();
            
            // names of columns
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }
            return columnNames;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return null;
    }
}
