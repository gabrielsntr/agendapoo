package Model;

public class Medico {
    
    private int id;
    private String nome;
    private String nomeReduzido;
    private String crm;
    private boolean isAtivo;

    public Medico() {
    }

    public Medico(int id, String nome, String nomeReduzido, String crm, boolean isAtivo) {
        this.id = id;
        this.nome = nome;
        this.nomeReduzido = nomeReduzido;
        this.crm = crm;
        this.isAtivo = isAtivo;
    }

    public Medico(String nome, String nomeReduzido, String crm, boolean isAtivo) {
        this.nome = nome;
        this.nomeReduzido = nomeReduzido;
        this.crm = crm;
        this.isAtivo = isAtivo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeReduzido() {
        return nomeReduzido;
    }

    public void setNomeReduzido(String nomeReduzido) {
        this.nomeReduzido = nomeReduzido;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public boolean isIsAtivo() {
        return isAtivo;
    }

    public void setIsAtivo(boolean isAtivo) {
        this.isAtivo = isAtivo;
    }
    
    
}
