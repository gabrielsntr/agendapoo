package Model;

import java.util.Date;

public class Calendario {
    private Date data;
    private int dia;
    private int mes_int;
    private String mes;
    private int ano;
    private int dia_semana_int;
    private String dia_semana;

    public Calendario() {
    }

    public Calendario(Date data, int dia, int mes_int, String mes, int ano, int dia_semana_int, String dia_semana) {
        this.data = data;
        this.dia = dia;
        this.mes_int = mes_int;
        this.mes = mes;
        this.ano = ano;
        this.dia_semana_int = dia_semana_int;
        this.dia_semana = dia_semana;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes_int() {
        return mes_int;
    }

    public void setMes_int(int mes_int) {
        this.mes_int = mes_int;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getDia_semana_int() {
        return dia_semana_int;
    }

    public void setDia_semana_int(int dia_semana_int) {
        this.dia_semana_int = dia_semana_int;
    }

    public String getDia_semana() {
        return dia_semana;
    }

    public void setDia_semana(String dia_semana) {
        this.dia_semana = dia_semana;
    }
    
}
