package Model;

import java.sql.Time;
import java.sql.Date;

public class Agenda {
    private int id;
    private String descricao;
    private int id_medico;
    private int id_especialidade;
    private Date data;
    private Time hora;
    private String observacao;
    private int id_paciente;

    public Agenda() {
    }

    public Agenda(String descricao, int id_medico, int id_especialidade, Date data, Time hora, String observacao, int id_paciente) {
        this.descricao = descricao;
        this.id_medico = id_medico;
        this.id_especialidade = id_especialidade;
        this.data = data;
        this.hora = hora;
        this.observacao = observacao;
        this.id_paciente = id_paciente;
    }

    public Agenda(int id, String descricao, int id_medico, int id_especialidade, Date data, Time hora, String observacao, int id_paciente) {
        this.id = id;
        this.descricao = descricao;
        this.id_medico = id_medico;
        this.id_especialidade = id_especialidade;
        this.data = data;
        this.hora = hora;
        this.observacao = observacao;
        this.id_paciente = id_paciente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId_medico() {
        return id_medico;
    }

    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    public int getId_especialidade() {
        return id_especialidade;
    }

    public void setId_especialidade(int id_especialidade) {
        this.id_especialidade = id_especialidade;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(int id_paciente) {
        this.id_paciente = id_paciente;
    }

    

}
