package Model;

import java.sql.Date;

public class Paciente {
    private int id;
    private String nome;
    private String rg;
    private String cpf;
    private Date dataNascimento;
    private int sexo;
    private String profissao;
    private int estadoCivil;
    private String email;
    private String telefone;
    private String telefoneComercial;
    private String celular;
    private float peso;
    private float altura;
    private int tipo_sanguineo;
    private boolean isAlergico;
    private String alergico;
    private boolean isDoencaCronica;
    private String doencaCronica;
    private boolean isFumante;
    private boolean isCirurgiaAnterior;
    private String cirurgiaAnterior;
    private String observacao;

    public Paciente() {
    }


    public Paciente(int id, String nome, String rg, String cpf, Date dataNascimento, int sexo, String profissao, int estadoCivil, String email, String telefone, String telefoneComercial, String celular, float peso, float altura, int tipo_sanguineo, boolean isAlergico, String alergico, boolean isDoencaCronica, String doencaCronica, boolean isFumante, boolean isCirurgiaAnterior, String cirurgiaAnterior, String observacao) {
        this.id = id;
        this.nome = nome;
        this.rg = rg;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.profissao = profissao;
        this.estadoCivil = estadoCivil;
        this.email = email;
        this.telefone = telefone;
        this.telefoneComercial = telefoneComercial;
        this.celular = celular;
        this.peso = peso;
        this.altura = altura;
        this.tipo_sanguineo = tipo_sanguineo;
        this.isAlergico = isAlergico;
        this.alergico = alergico;
        this.isDoencaCronica = isDoencaCronica;
        this.doencaCronica = doencaCronica;
        this.isFumante = isFumante;
        this.isCirurgiaAnterior = isCirurgiaAnterior;
        this.cirurgiaAnterior = cirurgiaAnterior;
        this.observacao = observacao;
    }

    public Paciente(String nome, String rg, String cpf, Date dataNascimento, int sexo, String profissao, int estadoCivil, String email, String telefone, String telefoneComercial, String celular, float peso, float altura, int tipo_sanguineo, boolean isAlergico, String alergico, boolean isDoencaCronica, String doencaCronica, boolean isFumante, boolean isCirurgiaAnterior, String cirurgiaAnterior, String observacao) {
        this.nome = nome;
        this.rg = rg;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.profissao = profissao;
        this.estadoCivil = estadoCivil;
        this.email = email;
        this.telefone = telefone;
        this.telefoneComercial = telefoneComercial;
        this.celular = celular;
        this.peso = peso;
        this.altura = altura;
        this.tipo_sanguineo = tipo_sanguineo;
        this.isAlergico = isAlergico;
        this.alergico = alergico;
        this.isDoencaCronica = isDoencaCronica;
        this.doencaCronica = doencaCronica;
        this.isFumante = isFumante;
        this.isCirurgiaAnterior = isCirurgiaAnterior;
        this.cirurgiaAnterior = cirurgiaAnterior;
        this.observacao = observacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public int getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(int estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefoneComercial() {
        return telefoneComercial;
    }

    public void setTelefoneComercial(String telefoneComercial) {
        this.telefoneComercial = telefoneComercial;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public int getTipo_sanguineo() {
        return tipo_sanguineo;
    }

    public void setTipo_sanguineo(int tipo_sanguineo) {
        this.tipo_sanguineo = tipo_sanguineo;
    }

    public boolean isIsAlergico() {
        return isAlergico;
    }

    public void setIsAlergico(boolean isAlergico) {
        this.isAlergico = isAlergico;
    }

    public String getAlergico() {
        return alergico;
    }

    public void setAlergico(String alergico) {
        this.alergico = alergico;
    }

    public boolean isIsDoencaCronica() {
        return isDoencaCronica;
    }

    public void setIsDoencaCronica(boolean isDoencaCronica) {
        this.isDoencaCronica = isDoencaCronica;
    }

    public String getDoencaCronica() {
        return doencaCronica;
    }

    public void setDoencaCronica(String doencaCronica) {
        this.doencaCronica = doencaCronica;
    }

    public boolean isIsFumante() {
        return isFumante;
    }

    public void setIsFumante(boolean isFumante) {
        this.isFumante = isFumante;
    }

    public boolean isIsCirurgiaAnterior() {
        return isCirurgiaAnterior;
    }

    public void setIsCirurgiaAnterior(boolean isCirurgiaAnterior) {
        this.isCirurgiaAnterior = isCirurgiaAnterior;
    }

    public String getCirurgiaAnterior() {
        return cirurgiaAnterior;
    }

    public void setCirurgiaAnterior(String cirurgiaAnterior) {
        this.cirurgiaAnterior = cirurgiaAnterior;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
    
}
