package Controller;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Utils {
    
    public static String limpaNumero(String entrada){
        return entrada.replaceAll("[^0-9]", "");
    }
    
    public static String dateToString(Date data){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = df.format(data);
        return dataFormatada;
    }
    
    public static String dateToString(java.util.Date data){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = df.format(data);
        return dataFormatada;
    }

    public static Date stringToDate(String data) {
        DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
        Date dataFormatada;        
        try {
            dataFormatada = new Date(df1.parse(data).getTime());
            return dataFormatada;
        } catch (ParseException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static String horaToString(Time hora){
        DateFormat df = new SimpleDateFormat("HH:mm");
        String dataFormatada = df.format(hora);
        return dataFormatada;
    }    
    
    public static Time stringToHora(String data) {
        DateFormat df1 = new SimpleDateFormat("HH:mm");
        Time dataFormatada;        
        try {
            dataFormatada = new Time(df1.parse(data).getTime());
            return dataFormatada;
        } catch (ParseException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean comboBoxBoolean(int index){
        if (index == 1){
            return true;
        } else {
            return false;
        }
    }
    
    public static int comboBoxInt(boolean bool){
        if (bool){
            return 1;
        } else {
            return 0;
        }
    }
    
    
    public static int horaToIndexComboBox(Time hora, JComboBox combo){
        String hs = horaToString(hora);
        String h = hs.split(":")[0];
        for (int i = 0; i < combo.getItemCount(); i++){
            if (h.equals(combo.getItemAt(i).toString())){
                return i;
            }
        }
        return 0;
    }
    
        public static int minutoToIndexComboBox(Time hora, JComboBox combo){
        String hs = horaToString(hora);
        String h = hs.split(":")[1];
        for (int i = 0; i < combo.getItemCount(); i++){
            if (h.equals(combo.getItemAt(i).toString())){
                return i;
            }
        }
        return 0;
    }
    
    public static void filtrarTabela(JTable tabela, String texto, int coluna){
        TableRowSorter<TableModel> sorter;
        sorter = new TableRowSorter<TableModel>(tabela.getModel());
        tabela.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.regexFilter("(?i)" + texto, coluna));
    }  
    
        public static void ajustaColunasTabela(JTable tabela){
            
            for (int column = 0; column < tabela.getColumnCount(); column++){
                TableColumn tableColumn = tabela.getColumnModel().getColumn(column);
                int preferredWidth = tableColumn.getMinWidth();
                int maxWidth = 0;
                TableCellRenderer rend = tabela.getTableHeader().getDefaultRenderer();
                TableCellRenderer rendCol = tableColumn.getHeaderRenderer();
                if (rendCol == null) rendCol = rend;
                Component header = rendCol.getTableCellRendererComponent(tabela, tableColumn.getHeaderValue(), false, false, 0, column);
                maxWidth = header.getPreferredSize().width;

                for (int row = 0; row < tabela.getRowCount(); row++){
                    TableCellRenderer cellRenderer = tabela.getCellRenderer(row, column);
                    Component c = tabela.prepareRenderer(cellRenderer, row, column);
                    int width = c.getPreferredSize().width + tabela.getIntercellSpacing().width;
                    preferredWidth = Math.max(preferredWidth, width);

                    //  We've exceeded the maximum width, no need to check other rows

                    if (preferredWidth <= maxWidth){
                        preferredWidth = maxWidth;
                        break;
                    }
                }
                tableColumn.setPreferredWidth(preferredWidth);
            }        
    }

    
}
