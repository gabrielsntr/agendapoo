package Controller;

import DAO.UsuarioDAO;
import Model.Usuario;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTable;

public class ControllerCadastroUsuario {
    
    private UsuarioDAO dao = new UsuarioDAO();
    
    public ControllerCadastroUsuario() {
    }    

    public void preencheBusca(JComboBox comboBox){
        comboBox.removeAllItems();
        Vector<String> lista = dao.preencheComboBusca();
        for (String item : lista){
            comboBox.addItem(item);
        }
    }
    
    public void preencheTabela(JTable tabela){
        tabela.setModel(dao.preencheJTable());
        Controller.Utils.ajustaColunasTabela(tabela);
    }
    
    public Usuario selecionar(String id){
        return dao.selecionar(Integer.parseInt(id));
    }

    public void excluir(String id){
        dao.excluir(Integer.parseInt(id));
    }
    
    public void atualizar(Usuario u){
        dao.atualizar(u);
    }
    
    public void inserir(Usuario u){
        dao.inserir(u);
    }
    
}
