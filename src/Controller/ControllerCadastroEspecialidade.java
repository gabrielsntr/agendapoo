package Controller;

import DAO.EspecialidadeDAO;
import Model.Especialidade;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTable;

public class ControllerCadastroEspecialidade {
    
    private EspecialidadeDAO dao = new EspecialidadeDAO();
    
    public ControllerCadastroEspecialidade() {
    }    

    public void preencheBusca(JComboBox comboBox){
        comboBox.removeAllItems();
        Vector<String> lista = dao.preencheComboBusca();
        for (String item : lista){
            comboBox.addItem(item);
        }
    }
    
    public void preencheTabela(JTable tabela, int idMedico){
        tabela.setModel(dao.preencheJTable(idMedico));
        Controller.Utils.ajustaColunasTabela(tabela);
    }
    
    public Especialidade selecionar(String id){
        return dao.selecionar(Integer.parseInt(id));
    }

    public void excluir(String id){
        dao.excluir(Integer.parseInt(id));
    }
    
    public void atualizar(Especialidade e){
        dao.atualizar(e);
    }
    
    public void inserir(Especialidade e){
        dao.inserir(e);
    }

}
