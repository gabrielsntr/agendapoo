package View;

import DAO.AgendaDAO;
import DAO.MedicoDAO;
import Model.Agenda;
import Model.Medico;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import javax.swing.JOptionPane;

public class ViewAgenda extends javax.swing.JDialog {

    public AgendaDAO agenda = new AgendaDAO();
    public MedicoDAO medicos = new MedicoDAO();
    public boolean isAgendaCarregada = false;
    
    public void dataPadrao(){
        java.util.Date d1 = new java.util.Date();
        java.util.Date d2 = new java.util.Date();
        //adicionar 6 dias
        Calendar c = Calendar.getInstance(); 
        c.setTime(d2); 
        c.add(Calendar.DATE, 6);
        d2 = c.getTime();
        
        tfDataInicial.setText(Controller.Utils.dateToString(d1));
        tfDataFinal.setText(Controller.Utils.dateToString(d2));
    }
    
    
    public void preencheTabela(String d1, String d2){
        tbDados.setRowHeight(40);
        tbDados.setModel(agenda.preencheJTable(d1, d2));
        Controller.Utils.ajustaColunasTabela(tbDados);
        isAgendaCarregada = false;
        tbDados.setEnabled(false);
    }
    
    public void alimentaTabela(String d1, String d2, String idMedico){
        agenda.alimentaAgenda(tbDados, d1, d2, idMedico);
        Controller.Utils.ajustaColunasTabela(tbDados);
        isAgendaCarregada = true;
        tbDados.setEnabled(true);
    }
    
    public ViewAgenda(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tfNomeMedico.setFocusable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);        
        dataPadrao();
        preencheTabela(tfDataInicial.getText(), tfDataFinal.getText());

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbDados = new javax.swing.JTable();
        lblIdMedico = new javax.swing.JLabel();
        tfIdMedico = new javax.swing.JTextField();
        tfNomeMedico = new javax.swing.JTextField();
        lblDataInicial = new javax.swing.JLabel();
        tfDataInicial = new javax.swing.JFormattedTextField();
        lblDataFinal = new javax.swing.JLabel();
        tfDataFinal = new javax.swing.JFormattedTextField();
        btnCarregar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agenda");

        tbDados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbDados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbDadosMouseClicked(evt);
            }
        });
        tbDados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tbDadosKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(tbDados);

        lblIdMedico.setText("Médico:");

        tfIdMedico.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfIdMedicoFocusLost(evt);
            }
        });
        tfIdMedico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfIdMedicoMouseClicked(evt);
            }
        });
        tfIdMedico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfIdMedicoKeyTyped(evt);
            }
        });

        tfNomeMedico.setEditable(false);

        lblDataInicial.setText("Data Inicial:");

        try {
            tfDataInicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfDataInicial.setPreferredSize(new java.awt.Dimension(4, 24));

        lblDataFinal.setText("Data Final:");

        try {
            tfDataFinal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfDataFinal.setPreferredSize(new java.awt.Dimension(4, 24));

        btnCarregar.setText("Carregar");
        btnCarregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblIdMedico)
                        .addGap(39, 39, 39)
                        .addComponent(tfIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfNomeMedico))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblDataInicial)
                        .addGap(18, 18, 18)
                        .addComponent(tfDataInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDataFinal)
                        .addGap(18, 18, 18)
                        .addComponent(tfDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCarregar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIdMedico)
                    .addComponent(tfNomeMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDataInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDataInicial)
                    .addComponent(tfDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDataFinal)
                    .addComponent(btnCarregar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCarregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregarActionPerformed
        if ((!tfIdMedico.getText().isEmpty()) && !(Controller.Utils.limpaNumero(tfDataInicial.getText()).isEmpty()) && !(Controller.Utils.limpaNumero(tfDataFinal.getText()).isEmpty())){
            preencheTabela(tfDataInicial.getText(), tfDataFinal.getText());
            alimentaTabela(tfDataInicial.getText(), tfDataFinal.getText(), tfIdMedico.getText());
        } else {
            JOptionPane.showMessageDialog(this, "Preencha o ID do médico e data inicial e final!!", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnCarregarActionPerformed

    private void tfIdMedicoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfIdMedicoKeyTyped
        char c = evt.getKeyChar();
        if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
          getToolkit().beep();
          evt.consume();
        }
    }//GEN-LAST:event_tfIdMedicoKeyTyped

    private void tfIdMedicoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfIdMedicoMouseClicked
        if (evt.getClickCount() == 2) {
            ViewCadastroMedico medico = new ViewCadastroMedico(null, true);
            medico.isBusca = true;
            medico.setVisible(true);
            tfIdMedico.setText(medico.id_selecionado);
            int idMedico = Integer.parseInt(tfIdMedico.getText());
            Medico m = medicos.selecionar(idMedico);
            tfNomeMedico.setText(m.getNomeReduzido());            
        }
    }//GEN-LAST:event_tfIdMedicoMouseClicked

    private void tfIdMedicoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfIdMedicoFocusLost
        if (!tfIdMedico.getText().isEmpty()){
            int idMedico = Integer.parseInt(tfIdMedico.getText());
            Medico m = medicos.selecionar(idMedico);
            tfNomeMedico.setText(m.getNomeReduzido());
        }
    }//GEN-LAST:event_tfIdMedicoFocusLost

    private void tbDadosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbDadosKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tbDadosKeyTyped

    private void tbDadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbDadosMouseClicked
        Agenda a = null;
        Point point = evt.getPoint();
        int row = tbDados.rowAtPoint(point);
        int col = tbDados.columnAtPoint(point);
        ViewCadastroAgenda cadastro = null;
        if (evt.getClickCount() == 2 && (tbDados.getSelectedRow() != -1) && tbDados.getSelectedColumn() > 0) {
            Object cell = tbDados.getModel().getValueAt(row, col);
            if (cell == null){
                a = new Agenda();
                a.setData(Controller.Utils.stringToDate(tbDados.getModel().getColumnName(col)));
                a.setHora(Controller.Utils.stringToHora(tbDados.getValueAt(row, 0).toString()));
                a.setId_medico(Integer.parseInt(tfIdMedico.getText()));
                cadastro = new ViewCadastroAgenda(null, true, false, a);
                cadastro.isAtualizaCadastro = false;
            } else if (!cell.toString().isEmpty()){
                a = agenda.selecionar(tbDados.getModel().getColumnName(col), tbDados.getValueAt(row, 0).toString(), Integer.parseInt(tfIdMedico.getText()));
                cadastro = new ViewCadastroAgenda(null, true, true, a);
            } else {
                cadastro = new ViewCadastroAgenda(null, true, false, null);
                cadastro.isAtualizaCadastro = false;
            }
            cadastro.setVisible(true);
            btnCarregar.doClick();
        }
    }//GEN-LAST:event_tbDadosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ViewAgenda dialog = new ViewAgenda(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCarregar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDataFinal;
    private javax.swing.JLabel lblDataInicial;
    private javax.swing.JLabel lblIdMedico;
    private javax.swing.JTable tbDados;
    private javax.swing.JFormattedTextField tfDataFinal;
    private javax.swing.JFormattedTextField tfDataInicial;
    private javax.swing.JTextField tfIdMedico;
    private javax.swing.JTextField tfNomeMedico;
    // End of variables declaration//GEN-END:variables
}
