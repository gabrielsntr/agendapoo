package View;
import DAO.EspecialidadeDAO;
import DAO.MedicoDAO;
import Model.Medico;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class ViewCadastroMedico extends javax.swing.JDialog {

    private MedicoDAO medicos = new MedicoDAO();
    private EspecialidadeDAO especialidades = new EspecialidadeDAO();
    public boolean isAtualizaCadastro = false;
    public boolean isBusca;
    public String id_selecionado = "";
    
    public void desabilitaTudo(){
        //Desabilita
        btnCadastrar.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
        tfId.setEnabled(false);
        tfNome.setEnabled(false);
        tfNomeRed.setEnabled(false);
        tfCRM.setEnabled(false);
        cbAtivo.setEnabled(false);
        listEspecialidade.setEnabled(false);
        btnSalvar.setEnabled(false);
        btnCancelar.setEnabled(false);
        tfBusca.setEnabled(false);      
        btnBuscar.setEnabled(false);      
        cbBusca.setEnabled(false);      
        //Limpa
        tfId.setText("");
        tfNome.setText("");
        tfNomeRed.setText("");
        tfCRM.setText("");
        cbAtivo.setSelectedIndex(0);
        listEspecialidade.clearSelection();
        tfBusca.setText("");
        cbBusca.setSelectedIndex(0);
    }
    
    
    public void estadoInicial(){
        preencheTabela();
        preencheEspecialidade();
        btnCadastrar.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);
        tfBusca.setEnabled(true);
        btnBuscar.setEnabled(true);
        cbBusca.setEnabled(true);
    }    
    
    public void preencheCbBusca(){
        cbBusca.removeAllItems();
        Vector<String> lista = medicos.preencheComboBusca();
        for (String item : lista){
            cbBusca.addItem(item);
        }
    }

    public void habilitaCadastro(){
        tfId.setEnabled(true);
        tfNome.setEnabled(true);
        tfNomeRed.setEnabled(true);
        tfCRM.setEnabled(true);
        cbAtivo.setEnabled(true);
        listEspecialidade.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSalvar.setEnabled(true);
    }    
    
      public void preencheTabela(){
        tbDados.setModel(medicos.preencheJTable());
        Controller.Utils.ajustaColunasTabela(tbDados);
    }
      
    public void preencheEspecialidade(){
          
          Vector<String> espec = especialidades.selecionarListaMedicos();
          DefaultListModel model = new DefaultListModel();
          for(String item : espec){
              model.addElement(item);
          }
          listEspecialidade.setModel(model);
      }
    
    public ViewCadastroMedico(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        preencheCbBusca();
        desabilitaTudo();
        estadoInicial();           
    }
    
    public void marcaItensLista(int id){
        int[] especialidades = medicos.selecionarEspecialidade(id);
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(int i = 0; i < listEspecialidade.getModel().getSize(); i++){
            int idLista = Integer.parseInt(listEspecialidade.getModel().getElementAt(i).split(" - ")[0]);
            for(int j : especialidades){
                if(j == idLista){
                    ids.add(i);
                    //System.out.println("Lista = " + i + "; Especialidade Médico = " + j);
                }
            }
        }
        int[] selecao = new int[ids.size()];
        for (int i = 0; i < ids.size(); i++){
            selecao[i] = ids.get(i);
        }
        listEspecialidade.clearSelection();
        listEspecialidade.setSelectedIndices(selecao);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAlterar = new javax.swing.JButton();
        btnCadastrar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        lblTitulo = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        tfNome = new javax.swing.JTextField();
        tfNomeRed = new javax.swing.JTextField();
        lblNomeRed = new javax.swing.JLabel();
        tfCRM = new javax.swing.JTextField();
        lblCRM = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listEspecialidade = new javax.swing.JList<>();
        lblEspecialidade = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        cbBusca = new javax.swing.JComboBox<>();
        tfBusca = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbDados = new javax.swing.JTable();
        tfId = new javax.swing.JTextField();
        lblId = new javax.swing.JLabel();
        lblAtivo = new javax.swing.JLabel();
        cbAtivo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Médico");

        btnAlterar.setText("Alterar");
        btnAlterar.setPreferredSize(new java.awt.Dimension(87, 32));
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnCadastrar.setText("Cadastrar");
        btnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.setPreferredSize(new java.awt.Dimension(87, 32));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        lblTitulo.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("CADASTRO DE MÉDICO");

        lblNome.setText("Nome:");

        lblNomeRed.setText("Nome Reduzido:");

        lblCRM.setText("CRM:");

        listEspecialidade.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(listEspecialidade);

        lblEspecialidade.setText("Especialidades:");

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        cbBusca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1" }));

        tfBusca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfBuscaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfBuscaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfBuscaKeyTyped(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        tbDados.setAutoCreateRowSorter(true);
        tbDados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbDados.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbDados.getTableHeader().setReorderingAllowed(false);
        tbDados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbDadosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbDados);

        tfId.setEditable(false);

        lblId.setText("ID");

        lblAtivo.setText("Ativo:");

        cbAtivo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Não", "Sim" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfBusca)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCadastrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(88, 88, 88))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblId)
                            .addComponent(lblNome))
                        .addGap(69, 69, 69)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfNome, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfId, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNomeRed)
                            .addComponent(lblCRM))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfCRM, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblAtivo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbAtivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfNomeRed)
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblEspecialidade)
                        .addGap(16, 16, 16)
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNome))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfNomeRed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNomeRed))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfCRM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCRM)
                    .addComponent(lblAtivo)
                    .addComponent(cbAtivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEspecialidade))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        int selecionado = 0;
        Medico m;
        if (tbDados.getSelectedRow() >= 0){
            desabilitaTudo();
            habilitaCadastro();
            selecionado = Integer.parseInt(tbDados.getValueAt(tbDados.getSelectedRow(), 0).toString());
            m = medicos.selecionar(selecionado);
            marcaItensLista(selecionado);
            tfId.setText(String.valueOf(m.getId()));
            tfNome.setText(m.getNome());
            tfNomeRed.setText(m.getNomeReduzido());
            tfCRM.setText(m.getCrm());
            cbAtivo.setSelectedIndex(Controller.Utils.comboBoxInt(m.isIsAtivo()));
            isAtualizaCadastro = true;
            tfNome.requestFocus();
        } else {
            JOptionPane.showMessageDialog(this, "Por favor, selecione o registro a ser alterado!", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarActionPerformed
        desabilitaTudo();
        habilitaCadastro();
        tfNome.requestFocus();
        isAtualizaCadastro = false;
    }//GEN-LAST:event_btnCadastrarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        int selecionado = 0;
        if (tbDados.getSelectedRow() >= 0){
            selecionado = Integer.parseInt(tbDados.getValueAt(tbDados.getSelectedRow(), 0).toString());
            if (JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o registro selecionado?", "Aviso", JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION){
                medicos.excluirEspecialidade(selecionado);
                medicos.excluir(selecionado);                
                estadoInicial();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Por favor, selecione o registro a ser excluído!", "Aviso", JOptionPane.WARNING_MESSAGE);

        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if (tfNome.getText().trim() == "" || tfNome.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "O campo de nome deve ser preenchido!", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else{
            if (isAtualizaCadastro){
                Medico p = new Medico(Integer.parseInt(tfId.getText()),
                    tfNome.getText(),
                    tfNomeRed.getText(),
                    tfCRM.getText(),
                    Controller.Utils.comboBoxBoolean(cbAtivo.getSelectedIndex())
                );
                medicos.atualizar(p);
                medicos.excluirEspecialidade(p.getId());
                int[] especialidades = listEspecialidade.getSelectedIndices();
                for(int item : especialidades){
                    System.out.println(item);
                    int id_esp = Integer.parseInt(listEspecialidade.getModel().getElementAt(item).split(" - ")[0]);
                    medicos.inserirEspecialidades(p.getId(), id_esp);
                }                
                
            } else {
                int inserted = 0;
                Medico p = new Medico(tfNome.getText(),
                    tfNomeRed.getText(),
                    tfCRM.getText(),
                    Controller.Utils.comboBoxBoolean(cbAtivo.getSelectedIndex()));
                inserted = medicos.inserir(p);
                int[] especialidades = listEspecialidade.getSelectedIndices();
                for(int item : especialidades){
                    int id_esp = Integer.parseInt(listEspecialidade.getModel().getElementAt(item).split(" - ")[0]);
                    medicos.inserirEspecialidades(inserted, id_esp);
                }
                
                    
            }
            desabilitaTudo();
            estadoInicial();
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        desabilitaTudo(); 
        estadoInicial();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void tfBuscaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBuscaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            btnBuscar.doClick();
        }
    }//GEN-LAST:event_tfBuscaKeyPressed

    private void tfBuscaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBuscaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tfBuscaKeyReleased

    private void tfBuscaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBuscaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tfBuscaKeyTyped

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        String texto = tfBusca.getText();
        Controller.Utils.filtrarTabela(tbDados, texto, cbBusca.getSelectedIndex());
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void tbDadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbDadosMouseClicked
        Point point = evt.getPoint();
        int row = tbDados.rowAtPoint(point);
        if (evt.getClickCount() == 2 && (tbDados.getSelectedRow() != -1) && isBusca) {
            id_selecionado = tbDados.getModel().getValueAt(tbDados.convertRowIndexToModel(row), 0).toString();
            this.dispose();
        }
    }//GEN-LAST:event_tbDadosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ViewCadastroMedico dialog = new ViewCadastroMedico(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCadastrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cbAtivo;
    private javax.swing.JComboBox<String> cbBusca;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblAtivo;
    private javax.swing.JLabel lblCRM;
    private javax.swing.JLabel lblEspecialidade;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNomeRed;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JList<String> listEspecialidade;
    private javax.swing.JTable tbDados;
    private javax.swing.JTextField tfBusca;
    private javax.swing.JTextField tfCRM;
    private javax.swing.JTextField tfId;
    private javax.swing.JTextField tfNome;
    private javax.swing.JTextField tfNomeRed;
    // End of variables declaration//GEN-END:variables
}
