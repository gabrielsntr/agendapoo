/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.PacienteDAO;
import Model.Paciente;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Vector;
import javax.swing.JOptionPane;

public class ViewCadastroPaciente extends javax.swing.JDialog {

    private PacienteDAO pacientes = new PacienteDAO();
    public boolean isAtualizaCadastro = false;
    public boolean isBusca;
    public String id_selecionado = "";

    
    public void desabilitaTudo(){
        //Desabilita
        btnCadastrar.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
        tfId.setEnabled(false);
        tfNome.setEnabled(false);
        tfRg.setEnabled(false);
        tfCpf.setEnabled(false);
        tfDataNascimento.setEnabled(false);
        cbSexo.setEnabled(false);
        cbEstadoCivil.setEnabled(false);
        tfProfissao.setEnabled(false);
        tfEmail.setEnabled(false);
        tfTelefone.setEnabled(false);
        tfTelefoneComercial.setEnabled(false);
        tfCelular.setEnabled(false);
        tfPeso.setEnabled(false);
        tfAltura.setEnabled(false);
        cbTipoSanguineo.setEnabled(false);
        cbAlergico.setEnabled(false);
        tfAlergia.setEnabled(false);
        cbDoenca.setEnabled(false);
        tfDoencaCronica.setEnabled(false);
        cbFumante.setEnabled(false);
        cbCirurgia.setEnabled(false);
        tfCirurgiaAnt.setEnabled(false);
        taObservacoes.setEnabled(false);
        btnSalvar.setEnabled(false);
        btnCancelar.setEnabled(false);
        tfBusca.setEnabled(false);      
        btnBuscar.setEnabled(false);      
        cbBusca.setEnabled(false);      
        //Limpa
        tfId.setText("");
        tfNome.setText("");
        tfRg.setText("");
        tfCpf.setText("");
        tfDataNascimento.setText("");
        cbSexo.setSelectedIndex(0);
        cbEstadoCivil.setSelectedIndex(0);
        tfProfissao.setText("");
        tfEmail.setText("");
        tfTelefone.setText("");
        tfTelefoneComercial.setText("");
        tfCelular.setText("");
        tfPeso.setText("");
        tfAltura.setText("");
        cbTipoSanguineo.setSelectedIndex(0);
        cbAlergico.setSelectedIndex(0);
        tfAlergia.setText("");
        cbDoenca.setSelectedIndex(0);
        tfDoencaCronica.setText("");
        cbFumante.setSelectedIndex(0);
        cbCirurgia.setSelectedIndex(0);
        tfCirurgiaAnt.setText("");
        taObservacoes.setText("");
        tfBusca.setText("");
        cbBusca.setSelectedIndex(0);
    }
    
    public void estadoInicial(){
        preencheTabela();
        btnCadastrar.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);
        tfBusca.setEnabled(true);
        btnBuscar.setEnabled(true);
        cbBusca.setEnabled(true);
    }
    
    public void preencheTabela(){
        tbDados.setModel(pacientes.preencheJTable());
        Controller.Utils.ajustaColunasTabela(tbDados);
    }
    
    public void preencheCbBusca(){
        cbBusca.removeAllItems();
        Vector<String> lista = pacientes.preencheComboBusca();
        for (String item : lista){
            cbBusca.addItem(item);
        }
    }
    
    public void habilitaCadastro(){
        tfId.setEnabled(true);
        tfNome.setEnabled(true);
        tfRg.setEnabled(true);
        tfCpf.setEnabled(true);
        tfDataNascimento.setEnabled(true);
        cbSexo.setEnabled(true);
        cbEstadoCivil.setEnabled(true);
        tfProfissao.setEnabled(true);
        tfEmail.setEnabled(true);
        tfTelefone.setEnabled(true);
        tfTelefoneComercial.setEnabled(true);
        tfCelular.setEnabled(true);
        tfPeso.setEnabled(true);
        tfAltura.setEnabled(true);
        cbTipoSanguineo.setEnabled(true);
        cbAlergico.setEnabled(true);
        tfAlergia.setEnabled(true);
        cbDoenca.setEnabled(true);
        tfDoencaCronica.setEnabled(true);
        cbFumante.setEnabled(true);
        cbCirurgia.setEnabled(true);
        tfCirurgiaAnt.setEnabled(true);
        taObservacoes.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSalvar.setEnabled(true);
    }
    
    private void liberaCampos(){
        if (cbAlergico.getSelectedIndex() == 1){
            tfAlergia.setEditable(true);
        } else{
            tfAlergia.setEditable(false);
        }
        if (cbDoenca.getSelectedIndex() == 1){
            tfDoencaCronica.setEditable(true);
        } else{
            tfDoencaCronica.setEditable(false);
        }
        if (cbCirurgia.getSelectedIndex() == 1){
            tfCirurgiaAnt.setEditable(true);
        } else{
            tfCirurgiaAnt.setEditable(false);
        }        
    }
    
    
    public ViewCadastroPaciente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        preencheCbBusca();
        desabilitaTudo();
        estadoInicial();         
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        lblTitulo = new javax.swing.JLabel();
        btnCadastrar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelDadosPessoais = new javax.swing.JPanel();
        lblId = new javax.swing.JLabel();
        tfId = new javax.swing.JTextField();
        tfNome = new javax.swing.JTextField();
        lblId1 = new javax.swing.JLabel();
        tfRg = new javax.swing.JTextField();
        tfCpf = new javax.swing.JFormattedTextField();
        tfDataNascimento = new javax.swing.JFormattedTextField();
        lblRg = new javax.swing.JLabel();
        lblCpf = new javax.swing.JLabel();
        lblDtNasc = new javax.swing.JLabel();
        cbSexo = new javax.swing.JComboBox<>();
        lblSexo = new javax.swing.JLabel();
        tfProfissao = new javax.swing.JTextField();
        lblProfissao = new javax.swing.JLabel();
        cbEstadoCivil = new javax.swing.JComboBox<>();
        lblEstadoCivil = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        tfEmail = new javax.swing.JTextField();
        tfTelefone = new javax.swing.JFormattedTextField();
        lblTelefone = new javax.swing.JLabel();
        lblTelefoneComercial = new javax.swing.JLabel();
        tfTelefoneComercial = new javax.swing.JFormattedTextField();
        lblCelular = new javax.swing.JLabel();
        tfCelular = new javax.swing.JFormattedTextField();
        PanelFicha = new javax.swing.JPanel();
        tfPeso = new javax.swing.JTextField();
        lblPeso = new javax.swing.JLabel();
        tfAltura = new javax.swing.JTextField();
        lblAltura = new javax.swing.JLabel();
        cbTipoSanguineo = new javax.swing.JComboBox<>();
        lblTipoSang = new javax.swing.JLabel();
        lblAlergico = new javax.swing.JLabel();
        cbAlergico = new javax.swing.JComboBox<>();
        lblAlergia = new javax.swing.JLabel();
        tfAlergia = new javax.swing.JTextField();
        lblDoenca = new javax.swing.JLabel();
        cbDoenca = new javax.swing.JComboBox<>();
        lblDoencaCronica = new javax.swing.JLabel();
        tfDoencaCronica = new javax.swing.JTextField();
        lblFumante = new javax.swing.JLabel();
        cbFumante = new javax.swing.JComboBox<>();
        cbCirurgia = new javax.swing.JComboBox<>();
        lblCirurgia = new javax.swing.JLabel();
        lblCirurgiaAnt = new javax.swing.JLabel();
        tfCirurgiaAnt = new javax.swing.JTextField();
        lblObservacoes = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taObservacoes = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbDados = new javax.swing.JTable();
        cbBusca = new javax.swing.JComboBox<>();
        tfBusca = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Pacientes");
        setResizable(false);

        lblTitulo.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("CADASTRO DE PACIENTE");

        btnCadastrar.setText("Cadastrar");
        btnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarActionPerformed(evt);
            }
        });

        btnAlterar.setText("Alterar");
        btnAlterar.setPreferredSize(new java.awt.Dimension(87, 32));
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.setPreferredSize(new java.awt.Dimension(87, 32));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        panelDadosPessoais.setBorder(javax.swing.BorderFactory.createCompoundBorder());

        lblId.setText("ID");

        tfId.setEditable(false);

        lblId1.setText("Nome:");

        try {
            tfCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfCpf.setPreferredSize(new java.awt.Dimension(4, 24));

        try {
            tfDataNascimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfDataNascimento.setPreferredSize(new java.awt.Dimension(4, 24));

        lblRg.setText("RG:");

        lblCpf.setText("CPF:");

        lblDtNasc.setText("Data Nasc.:");

        cbSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Masculino", "Feminino" }));

        lblSexo.setText("Sexo:");

        lblProfissao.setText("Profissão:");

        cbEstadoCivil.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Solteiro", "Casado", "Divorciado", "Viúvo", "Separado", "União Estável" }));
        cbEstadoCivil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbEstadoCivilActionPerformed(evt);
            }
        });

        lblEstadoCivil.setText("Estado Civil:");

        lblEmail.setText("E-mail:");

        try {
            tfTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) ####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfTelefone.setPreferredSize(new java.awt.Dimension(4, 24));

        lblTelefone.setText("Telefone:");

        lblTelefoneComercial.setText("Telefone Comercial:");

        try {
            tfTelefoneComercial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) ####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfTelefoneComercial.setPreferredSize(new java.awt.Dimension(4, 24));

        lblCelular.setText("Celular:");

        try {
            tfCelular.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) #####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfCelular.setPreferredSize(new java.awt.Dimension(4, 24));

        javax.swing.GroupLayout panelDadosPessoaisLayout = new javax.swing.GroupLayout(panelDadosPessoais);
        panelDadosPessoais.setLayout(panelDadosPessoaisLayout);
        panelDadosPessoaisLayout.setHorizontalGroup(
            panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                        .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblId)
                            .addComponent(lblId1)
                            .addComponent(lblRg)
                            .addComponent(lblDtNasc))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfNome, javax.swing.GroupLayout.PREFERRED_SIZE, 486, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                                .addComponent(tfRg, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblCpf)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                                .addComponent(tfDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblSexo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblEstadoCivil)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(tfId, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDadosPessoaisLayout.createSequentialGroup()
                        .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblProfissao)
                            .addComponent(lblEmail)
                            .addComponent(lblTelefone))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(tfEmail)
                                .addComponent(tfProfissao, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE))
                            .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                                .addComponent(tfTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTelefoneComercial)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfTelefoneComercial, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                        .addComponent(lblCelular)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tfCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(372, 372, 372))))
        );
        panelDadosPessoaisLayout.setVerticalGroup(
            panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDadosPessoaisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblId1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfRg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRg)
                    .addComponent(lblCpf))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblEstadoCivil)
                        .addComponent(cbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tfDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblDtNasc)
                        .addComponent(lblSexo)
                        .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfProfissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblProfissao))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEmail))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelefone)
                    .addComponent(lblTelefoneComercial)
                    .addComponent(tfTelefoneComercial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDadosPessoaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCelular))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Dados Pessoais", panelDadosPessoais);

        tfPeso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfPesoKeyTyped(evt);
            }
        });

        lblPeso.setText("Peso (kg):");

        tfAltura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfAlturaKeyTyped(evt);
            }
        });

        lblAltura.setText("Altura (m):");

        cbTipoSanguineo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Não informado", "A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-" }));

        lblTipoSang.setText("Tipo Sanguíneo:");

        lblAlergico.setText("Alérgico:");

        cbAlergico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Não", "Sim" }));
        cbAlergico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAlergicoActionPerformed(evt);
            }
        });

        lblAlergia.setText("Qual?");

        tfAlergia.setEditable(false);

        lblDoenca.setText("Doença Crônica:");

        cbDoenca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Não", "Sim" }));
        cbDoenca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDoencaActionPerformed(evt);
            }
        });

        lblDoencaCronica.setText("Qual?");

        tfDoencaCronica.setEditable(false);

        lblFumante.setText("Fumante:");

        cbFumante.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Não", "Sim" }));

        cbCirurgia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Não", "Sim" }));
        cbCirurgia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbCirurgiaActionPerformed(evt);
            }
        });

        lblCirurgia.setText("Cirurgia Anterior:");

        lblCirurgiaAnt.setText("Qual?");

        tfCirurgiaAnt.setEditable(false);

        lblObservacoes.setText("Observações:");

        taObservacoes.setColumns(20);
        taObservacoes.setRows(5);
        jScrollPane2.setViewportView(taObservacoes);

        javax.swing.GroupLayout PanelFichaLayout = new javax.swing.GroupLayout(PanelFicha);
        PanelFicha.setLayout(PanelFichaLayout);
        PanelFichaLayout.setHorizontalGroup(
            PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelFichaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPeso)
                    .addComponent(lblTipoSang)
                    .addComponent(lblAlergico)
                    .addComponent(lblDoenca)
                    .addComponent(lblFumante)
                    .addComponent(lblCirurgia)
                    .addComponent(lblObservacoes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbFumante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelFichaLayout.createSequentialGroup()
                        .addComponent(cbAlergico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblAlergia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfAlergia, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelFichaLayout.createSequentialGroup()
                        .addComponent(tfPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblAltura)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbTipoSanguineo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelFichaLayout.createSequentialGroup()
                        .addComponent(cbDoenca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDoencaCronica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfDoencaCronica, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelFichaLayout.createSequentialGroup()
                        .addComponent(cbCirurgia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblCirurgiaAnt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfCirurgiaAnt))
                    .addComponent(jScrollPane2))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        PanelFichaLayout.setVerticalGroup(
            PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelFichaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPeso)
                    .addComponent(tfAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAltura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbTipoSanguineo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoSang))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbAlergico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAlergico)
                    .addComponent(lblAlergia)
                    .addComponent(tfAlergia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDoenca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDoenca)
                    .addComponent(lblDoencaCronica)
                    .addComponent(tfDoencaCronica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbFumante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFumante))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbCirurgia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCirurgia)
                    .addComponent(lblCirurgiaAnt)
                    .addComponent(tfCirurgiaAnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblObservacoes)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Ficha Médica", PanelFicha);

        tbDados.setAutoCreateRowSorter(true);
        tbDados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbDados.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbDados.getTableHeader().setReorderingAllowed(false);
        tbDados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbDadosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbDados);

        cbBusca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1" }));

        tfBusca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfBuscaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfBuscaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfBuscaKeyTyped(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfBusca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 570, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(8, 8, 8))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 570, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(159, 159, 159)
                                .addComponent(btnCadastrar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(194, 194, 194))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbEstadoCivilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbEstadoCivilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbEstadoCivilActionPerformed

    private void btnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarActionPerformed
        desabilitaTudo();
        habilitaCadastro();
        tfNome.requestFocus();
        isAtualizaCadastro = false;
    }//GEN-LAST:event_btnCadastrarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        String texto = tfBusca.getText();
        Controller.Utils.filtrarTabela(tbDados, texto, cbBusca.getSelectedIndex());
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void tfBuscaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBuscaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tfBuscaKeyReleased

    private void tfBuscaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBuscaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tfBuscaKeyTyped

    private void tfBuscaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBuscaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            btnBuscar.doClick();
        }
    }//GEN-LAST:event_tfBuscaKeyPressed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        desabilitaTudo();
        estadoInicial();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        int selecionado = 0;
        Paciente p;
        if (tbDados.getSelectedRow() >= 0){
            desabilitaTudo();
            habilitaCadastro();            
            selecionado = Integer.parseInt(tbDados.getValueAt(tbDados.getSelectedRow(), 0).toString());
            p = pacientes.selecionar(selecionado);
            tfId.setText(String.valueOf(p.getId()));
            tfNome.setText(p.getNome());
            tfRg.setText(p.getRg());
            tfCpf.setText(p.getCpf());
            tfDataNascimento.setText(Controller.Utils.dateToString(p.getDataNascimento()));
            cbSexo.setSelectedIndex(p.getSexo());
            cbEstadoCivil.setSelectedIndex(p.getEstadoCivil());
            tfProfissao.setText(p.getProfissao());
            tfEmail.setText(p.getEmail());
            tfTelefone.setText(p.getTelefone());
            tfTelefoneComercial.setText(p.getTelefoneComercial());
            tfCelular.setText(p.getCelular());
            tfPeso.setText(String.valueOf(p.getPeso()));
            tfAltura.setText(String.valueOf(p.getAltura()));
            cbTipoSanguineo.setSelectedIndex(p.getTipo_sanguineo());
            cbAlergico.setSelectedIndex(Controller.Utils.comboBoxInt(p.isIsAlergico()));
            tfAlergia.setText(p.getAlergico());
            cbDoenca.setSelectedIndex(Controller.Utils.comboBoxInt(p.isIsDoencaCronica()));
            tfDoencaCronica.setText(p.getDoencaCronica());
            cbFumante.setSelectedIndex(Controller.Utils.comboBoxInt(p.isIsFumante()));
            cbCirurgia.setSelectedIndex(Controller.Utils.comboBoxInt(p.isIsCirurgiaAnterior()));
            tfCirurgiaAnt.setText(p.getCirurgiaAnterior());
            taObservacoes.setText(p.getObservacao());
            isAtualizaCadastro = true;        
            tfNome.requestFocus();
        } else {
            JOptionPane.showMessageDialog(this, "Por favor, selecione o registro a ser alterado!", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if (tfNome.getText().trim() == "" || tfNome.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "O campo de nome e data de nascimento devem ser preenchidos!", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else{
            if (isAtualizaCadastro){
                Paciente p = new Paciente(Integer.parseInt(tfId.getText()),
                        tfNome.getText(), 
                        tfRg.getText(), 
                        Controller.Utils.limpaNumero(tfCpf.getText()), 
                        Controller.Utils.stringToDate(tfDataNascimento.getText()), 
                        cbSexo.getSelectedIndex(), 
                        tfProfissao.getText(), 
                        cbEstadoCivil.getSelectedIndex(), 
                        tfEmail.getText(), 
                        Controller.Utils.limpaNumero(tfTelefone.getText()), 
                        Controller.Utils.limpaNumero(tfTelefoneComercial.getText()), 
                        Controller.Utils.limpaNumero(tfCelular.getText()), 
                        Float.parseFloat(tfPeso.getText().replace(',', '.')), 
                        Float.parseFloat(tfAltura.getText().replace(',', '.')),
                        cbTipoSanguineo.getSelectedIndex(), 
                        Controller.Utils.comboBoxBoolean(cbAlergico.getSelectedIndex()), 
                        tfAlergia.getText(), 
                        Controller.Utils.comboBoxBoolean(cbDoenca.getSelectedIndex()), 
                        tfDoencaCronica.getText(), 
                        Controller.Utils.comboBoxBoolean(cbFumante.getSelectedIndex()), 
                        Controller.Utils.comboBoxBoolean(cbCirurgia.getSelectedIndex()), 
                        tfCirurgiaAnt.getText(), 
                        taObservacoes.getText());
                pacientes.atualizar(p);                 
            } else {
                Paciente p = new Paciente(tfNome.getText(), 
                        tfRg.getText(), 
                        Controller.Utils.limpaNumero(tfCpf.getText()), 
                        Controller.Utils.stringToDate(tfDataNascimento.getText()), 
                        cbSexo.getSelectedIndex(), 
                        tfProfissao.getText(), 
                        cbEstadoCivil.getSelectedIndex(), 
                        tfEmail.getText(), 
                        Controller.Utils.limpaNumero(tfTelefone.getText()), 
                        Controller.Utils.limpaNumero(tfTelefoneComercial.getText()), 
                        Controller.Utils.limpaNumero(tfCelular.getText()), 
                        Float.parseFloat(tfPeso.getText().replace(',', '.')), 
                        Float.parseFloat(tfAltura.getText().replace(',', '.')),
                        cbTipoSanguineo.getSelectedIndex(), 
                        Controller.Utils.comboBoxBoolean(cbAlergico.getSelectedIndex()), 
                        tfAlergia.getText(), 
                        Controller.Utils.comboBoxBoolean(cbDoenca.getSelectedIndex()), 
                        tfDoencaCronica.getText(), 
                        Controller.Utils.comboBoxBoolean(cbFumante.getSelectedIndex()), 
                        Controller.Utils.comboBoxBoolean(cbCirurgia.getSelectedIndex()), 
                        tfCirurgiaAnt.getText(), 
                        taObservacoes.getText());
                pacientes.inserir(p);                
            }
            desabilitaTudo();
            estadoInicial();
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void tfPesoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfPesoKeyTyped
      char c = evt.getKeyChar();
      if (!((c >= '0') && (c <= '9') || (c <= ',') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
        getToolkit().beep();
        evt.consume();
      }
    }//GEN-LAST:event_tfPesoKeyTyped

    private void tfAlturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfAlturaKeyTyped
      char c = evt.getKeyChar();
      if (!((c >= '0') && (c <= '9') || (c <= ',') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
        getToolkit().beep();
        evt.consume();
      }
    }//GEN-LAST:event_tfAlturaKeyTyped

    private void cbAlergicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAlergicoActionPerformed
        liberaCampos();
    }//GEN-LAST:event_cbAlergicoActionPerformed

    private void cbDoencaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDoencaActionPerformed
        liberaCampos();
    }//GEN-LAST:event_cbDoencaActionPerformed

    private void cbCirurgiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbCirurgiaActionPerformed
        liberaCampos();
    }//GEN-LAST:event_cbCirurgiaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        int selecionado = 0;
        if (tbDados.getSelectedRow() >= 0){
            selecionado = Integer.parseInt(tbDados.getValueAt(tbDados.getSelectedRow(), 0).toString());
            if (JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o registro selecionado?", "Aviso", JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION){
                pacientes.excluir(selecionado);
                estadoInicial();
            }            
        } else {
            JOptionPane.showMessageDialog(this, "Por favor, selecione o registro a ser excluído!", "Aviso", JOptionPane.WARNING_MESSAGE);
            
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void tbDadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbDadosMouseClicked
        Point point = evt.getPoint();
        int row = tbDados.rowAtPoint(point);
        if (evt.getClickCount() == 2 && (tbDados.getSelectedRow() != -1) && isBusca) {
            id_selecionado = tbDados.getModel().getValueAt(tbDados.convertRowIndexToModel(row), 0).toString();
            this.dispose();
        }
    }//GEN-LAST:event_tbDadosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewCadastroPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ViewCadastroPaciente dialog = new ViewCadastroPaciente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelFicha;
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCadastrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cbAlergico;
    private javax.swing.JComboBox<String> cbBusca;
    private javax.swing.JComboBox<String> cbCirurgia;
    private javax.swing.JComboBox<String> cbDoenca;
    private javax.swing.JComboBox<String> cbEstadoCivil;
    private javax.swing.JComboBox<String> cbFumante;
    private javax.swing.JComboBox<String> cbSexo;
    private javax.swing.JComboBox<String> cbTipoSanguineo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lblAlergia;
    private javax.swing.JLabel lblAlergico;
    private javax.swing.JLabel lblAltura;
    private javax.swing.JLabel lblCelular;
    private javax.swing.JLabel lblCirurgia;
    private javax.swing.JLabel lblCirurgiaAnt;
    private javax.swing.JLabel lblCpf;
    private javax.swing.JLabel lblDoenca;
    private javax.swing.JLabel lblDoencaCronica;
    private javax.swing.JLabel lblDtNasc;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEstadoCivil;
    private javax.swing.JLabel lblFumante;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblId1;
    private javax.swing.JLabel lblObservacoes;
    private javax.swing.JLabel lblPeso;
    private javax.swing.JLabel lblProfissao;
    private javax.swing.JLabel lblRg;
    private javax.swing.JLabel lblSexo;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblTelefoneComercial;
    private javax.swing.JLabel lblTipoSang;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JPanel panelDadosPessoais;
    private javax.swing.JTextArea taObservacoes;
    private javax.swing.JTable tbDados;
    private javax.swing.JTextField tfAlergia;
    private javax.swing.JTextField tfAltura;
    private javax.swing.JTextField tfBusca;
    private javax.swing.JFormattedTextField tfCelular;
    private javax.swing.JTextField tfCirurgiaAnt;
    private javax.swing.JFormattedTextField tfCpf;
    private javax.swing.JFormattedTextField tfDataNascimento;
    private javax.swing.JTextField tfDoencaCronica;
    private javax.swing.JTextField tfEmail;
    private javax.swing.JTextField tfId;
    private javax.swing.JTextField tfNome;
    private javax.swing.JTextField tfPeso;
    private javax.swing.JTextField tfProfissao;
    private javax.swing.JTextField tfRg;
    private javax.swing.JFormattedTextField tfTelefone;
    private javax.swing.JFormattedTextField tfTelefoneComercial;
    // End of variables declaration//GEN-END:variables
}
