package View;

import DAO.AgendaDAO;
import DAO.EspecialidadeDAO;
import DAO.MedicoDAO;
import DAO.PacienteDAO;
import Model.Agenda;
import Model.Especialidade;
import Model.Medico;
import Model.Paciente;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.Time;
import java.time.LocalTime;
import javax.swing.JOptionPane;

public class ViewCadastroAgenda extends javax.swing.JDialog {

    public boolean isAtualizaCadastro = false;
    public AgendaDAO agendas = new AgendaDAO();
    public Agenda agenda;
    
    public void desabilitaTudo(){
        //Desabilita
        btnCadastrar.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
        tfId.setEnabled(false);
        tfDesc.setEnabled(false);
        tfIdPaciente.setEnabled(false);
        tfNomePaciente.setEnabled(false);
        tfIdMedico.setEnabled(false);
        tfNomeMedico.setEnabled(false);
        tfIdEspec.setEnabled(false);
        tfDescEspec.setEnabled(false);        
        tfDataInicial.setEnabled(false);
        cbHoraInicial.setEnabled(false);
        cbHoraFinal.setEnabled(false);
        cbMinutoInicial.setEnabled(false);
        cbMinutoFinal.setEnabled(false);        
        taObs.setEnabled(false);
        btnSalvar.setEnabled(false);
        btnCancelar.setEnabled(false);
    }
    
    public void limpaCampos(){
        //Limpa
        tfId.setText("");
        tfDesc.setText("");
        tfIdPaciente.setText("");
        tfNomePaciente.setText("");
        tfIdMedico.setText("");
        tfNomeMedico.setText("");
        tfIdEspec.setText("");
        tfDescEspec.setText("");    
        tfDataInicial.setText("");
        cbHoraInicial.setSelectedIndex(0);
        cbHoraFinal.setSelectedIndex(0);
        cbMinutoInicial.setSelectedIndex(0);
        cbMinutoFinal.setSelectedIndex(0);
        taObs.setText("");        
    }
    
    
    public void estadoInicial(){
        LocalTime localtime = agenda.getHora().toLocalTime();
        localtime = localtime.plusMinutes(30);
        String output = localtime.toString();
        tfDesc.setText(agenda.getDescricao());        
        tfNomePaciente.setText("");
        tfIdMedico.setText(String.valueOf(agenda.getId_medico()));
        atualizaNomeMedico();
        tfDescEspec.setText("");
        tfDataInicial.setText(Controller.Utils.dateToString(agenda.getData()));
        cbHoraInicial.setSelectedIndex(Controller.Utils.horaToIndexComboBox(agenda.getHora(), cbHoraInicial));
        cbHoraFinal.setSelectedIndex(Controller.Utils.horaToIndexComboBox(Controller.Utils.stringToHora(output), cbHoraFinal));
        cbMinutoInicial.setSelectedIndex(Controller.Utils.minutoToIndexComboBox(agenda.getHora(), cbMinutoInicial));
        cbMinutoFinal.setSelectedIndex(Controller.Utils.minutoToIndexComboBox(Controller.Utils.stringToHora(output), cbMinutoFinal));        
        if(!isAtualizaCadastro){
            tfId.setText("");
            tfIdPaciente.setText("");
            tfIdEspec.setText("");
            taObs.setText("");
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            btnCadastrar.setEnabled(true);
        } else {
            tfId.setText(String.valueOf(agenda.getId()));
            tfIdPaciente.setText(String.valueOf(agenda.getId_paciente()));
            atualizaNomePaciente();
            tfIdEspec.setText(String.valueOf(agenda.getId_especialidade()));
            atualizaDescEspecialidade();
            taObs.setText(agenda.getObservacao());
            btnCadastrar.setEnabled(false);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
        }
    }    

    public void habilitaCadastro(){       
        tfId.setEnabled(true);
        tfDesc.setEnabled(true);
        tfIdPaciente.setEnabled(true);
        tfIdEspec.setEnabled(true);
        cbHoraFinal.setEnabled(true);
        cbMinutoFinal.setEnabled(true);        
        taObs.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSalvar.setEnabled(true);
    }    
    
    public void atualizaNomePaciente(){
        tfNomePaciente.setText("");
        Paciente p = new Paciente();
        PacienteDAO pDAO = new PacienteDAO();
        if (!tfIdPaciente.getText().isEmpty()){
            p = pDAO.selecionar(Integer.parseInt(tfIdPaciente.getText()));
            tfNomePaciente.setText(p.getNome());
        }            
    }

    public void atualizaNomeMedico(){
        tfNomeMedico.setText("");
        Medico m = new Medico();
        MedicoDAO mDAO = new MedicoDAO();
        if (!tfIdMedico.getText().isEmpty()){
            m = mDAO.selecionar(Integer.parseInt(tfIdMedico.getText()));
            tfNomeMedico.setText(m.getNome());
        }            
    }

    public void atualizaDescEspecialidade(){
        tfDescEspec.setText("");
        Especialidade e = new Especialidade();
        EspecialidadeDAO eDAO = new EspecialidadeDAO();
        if (!tfIdEspec.getText().isEmpty()){
            e = eDAO.selecionar(Integer.parseInt(tfIdEspec.getText()));
            tfDescEspec.setText(e.getDescricao());
        }            
    }

    public ViewCadastroAgenda(java.awt.Frame parent, boolean modal, boolean isAtualizaCadastro, Agenda a) {
        super(parent, modal);
        this.isAtualizaCadastro = isAtualizaCadastro;
        this.agenda = a;
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);        
        desabilitaTudo();
        estadoInicial();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitulo = new javax.swing.JLabel();
        btnCadastrar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        tfId = new javax.swing.JTextField();
        lblId = new javax.swing.JLabel();
        tfDesc = new javax.swing.JTextField();
        lblDesc = new javax.swing.JLabel();
        lblIdMed = new javax.swing.JLabel();
        tfIdMedico = new javax.swing.JTextField();
        tfNomeMedico = new javax.swing.JTextField();
        lblEspec = new javax.swing.JLabel();
        tfIdEspec = new javax.swing.JTextField();
        tfDescEspec = new javax.swing.JTextField();
        lblDataInicial = new javax.swing.JLabel();
        tfDataInicial = new javax.swing.JFormattedTextField();
        lblHoraInicial = new javax.swing.JLabel();
        lblHoraFinal = new javax.swing.JLabel();
        cbHoraInicial = new javax.swing.JComboBox<>();
        cbHoraFinal = new javax.swing.JComboBox<>();
        cbMinutoInicial = new javax.swing.JComboBox<>();
        lblHoraInicial1 = new javax.swing.JLabel();
        cbMinutoFinal = new javax.swing.JComboBox<>();
        lblHoraInicial2 = new javax.swing.JLabel();
        tfIdPaciente = new javax.swing.JTextField();
        tfNomePaciente = new javax.swing.JTextField();
        lblIdMed1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taObs = new javax.swing.JTextArea();
        lblDataFinal1 = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agendamento");
        setResizable(false);

        lblTitulo.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("AGENDAMENTO");

        btnCadastrar.setText("Cadastrar");
        btnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarActionPerformed(evt);
            }
        });

        btnAlterar.setText("Alterar");
        btnAlterar.setPreferredSize(new java.awt.Dimension(87, 32));
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.setPreferredSize(new java.awt.Dimension(87, 32));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        tfId.setEditable(false);

        lblId.setText("ID");

        lblDesc.setText("Descrição:");

        lblIdMed.setText("Médico:");

        tfNomeMedico.setEditable(false);
        tfNomeMedico.setFocusable(false);

        lblEspec.setText("Especialidade:");

        tfIdEspec.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfIdEspecFocusLost(evt);
            }
        });
        tfIdEspec.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfIdEspecMouseClicked(evt);
            }
        });
        tfIdEspec.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfIdEspecKeyTyped(evt);
            }
        });

        tfDescEspec.setEditable(false);
        tfDescEspec.setFocusable(false);

        lblDataInicial.setText("Data Inicial:");

        try {
            tfDataInicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tfDataInicial.setPreferredSize(new java.awt.Dimension(4, 24));

        lblHoraInicial.setText("Hora Inicial:");

        lblHoraFinal.setText("Hora Final:");

        cbHoraInicial.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18" }));

        cbHoraFinal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18" }));

        cbMinutoInicial.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "30" }));

        lblHoraInicial1.setText(" : ");

        cbMinutoFinal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "30" }));

        lblHoraInicial2.setText(" : ");

        tfIdPaciente.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfIdPacienteFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfIdPacienteFocusLost(evt);
            }
        });
        tfIdPaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfIdPacienteMouseClicked(evt);
            }
        });
        tfIdPaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfIdPacienteKeyTyped(evt);
            }
        });

        tfNomePaciente.setEditable(false);
        tfNomePaciente.setFocusable(false);

        lblIdMed1.setText("Paciente:");

        taObs.setColumns(20);
        taObs.setRows(5);
        jScrollPane1.setViewportView(taObs);

        lblDataFinal1.setText("Observações:");

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(lblIdMed)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tfIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(lblEspec)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfIdEspec, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfDescEspec)
                            .addComponent(tfNomeMedico)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDesc)
                            .addComponent(lblIdMed1))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfIdPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfNomePaciente))
                            .addComponent(tfDesc)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDataFinal1)
                            .addComponent(lblHoraInicial))
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbHoraInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblHoraInicial1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbMinutoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblHoraFinal)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblHoraInicial2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbMinutoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE))
                            .addComponent(jScrollPane1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblDataInicial)
                                .addGap(23, 23, 23)
                                .addComponent(tfDataInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(84, 84, 84)
                                        .addComponent(btnCadastrar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblId)
                                        .addGap(77, 77, 77)
                                        .addComponent(tfId, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(128, 128, 128))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDesc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIdPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIdMed1)
                    .addComponent(tfNomePaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIdMed)
                    .addComponent(tfNomeMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIdEspec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEspec)
                    .addComponent(tfDescEspec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDataInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDataInicial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbHoraInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblHoraInicial))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbMinutoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblHoraInicial1)
                        .addComponent(lblHoraFinal)
                        .addComponent(cbHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblHoraInicial2)
                        .addComponent(cbMinutoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDataFinal1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarActionPerformed
        desabilitaTudo();
        habilitaCadastro();
        tfDesc.requestFocus();
        isAtualizaCadastro = false;
    }//GEN-LAST:event_btnCadastrarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
        tfDesc.setEnabled(true);
        tfIdPaciente.setEnabled(true);
        tfIdEspec.setEnabled(true);
        taObs.setEnabled(true);
        btnSalvar.setEnabled(true);
        btnCancelar.setEnabled(true);
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        int selecionado = 0;
        selecionado = Integer.parseInt(tfId.getText());
        if (JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o registro selecionado?", "Aviso", JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION){
            agendas.excluir(selecionado);
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Por favor, selecione o registro a ser excluído!", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        Agenda a = new Agenda();
        Time horaInicial = Controller.Utils.stringToHora(cbHoraInicial.getSelectedItem().toString() + ":" + cbMinutoInicial.getSelectedItem().toString());
        Time horaFinal = Controller.Utils.stringToHora(cbHoraFinal.getSelectedItem().toString() + ":" + cbMinutoFinal.getSelectedItem().toString());
        if ((tfDesc.getText().trim() == "" || tfDesc.getText().isEmpty()) || 
                (tfIdPaciente.getText().trim() == "" || tfIdPaciente.getText().isEmpty()) || 
                (tfIdMedico.getText().trim() == "" || tfIdMedico.getText().isEmpty()) || 
                (tfIdEspec.getText().trim() == "" || tfIdEspec.getText().isEmpty()) || 
                (Controller.Utils.limpaNumero(tfDataInicial.getText()).trim() == "" || Controller.Utils.limpaNumero(tfDataInicial.getText()).isEmpty())
                ){            
            JOptionPane.showMessageDialog(this, "Os campos de descrição, Paciente, Médico, Especialidade e Data devem estar preenchidos!", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else if (horaInicial.after(horaFinal) || horaFinal.equals(horaInicial)){
            JOptionPane.showMessageDialog(this, "A hora final deve ser maior que a hora inicial!", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else{
            if (isAtualizaCadastro){
                a.setId(Integer.parseInt(tfId.getText()));
                a.setDescricao(tfDesc.getText());
                a.setId_paciente(Integer.parseInt(tfIdPaciente.getText()));
                a.setId_medico(Integer.parseInt(tfIdMedico.getText()));
                a.setId_especialidade(Integer.parseInt(tfIdEspec.getText()));
                a.setData(Controller.Utils.stringToDate(tfDataInicial.getText()));
                a.setHora(horaInicial);
                a.setObservacao(taObs.getText());
                agendas.atualizar(a);
                this.dispose();
            } else {
                Time horaAux = horaInicial;
                a.setDescricao(tfDesc.getText());
                a.setId_paciente(Integer.parseInt(tfIdPaciente.getText()));
                a.setId_medico(Integer.parseInt(tfIdMedico.getText()));
                a.setId_especialidade(Integer.parseInt(tfIdEspec.getText()));
                a.setData(Controller.Utils.stringToDate(tfDataInicial.getText()));
                a.setObservacao(taObs.getText());                 
                while (horaFinal.after(horaAux)){
                    a.setHora(horaAux);
                    agendas.inserir(a);
                    LocalTime localtime = horaAux.toLocalTime();
                    localtime = localtime.plusMinutes(30);
                    horaAux = Controller.Utils.stringToHora(localtime.toString());
                }                
                this.dispose();
            }
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        desabilitaTudo();
        estadoInicial();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void tfIdPacienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfIdPacienteKeyTyped
        char c = evt.getKeyChar();
        if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
          getToolkit().beep();
          evt.consume();
        }
    }//GEN-LAST:event_tfIdPacienteKeyTyped

    private void tfIdEspecKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfIdEspecKeyTyped
        char c = evt.getKeyChar();
        if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
          getToolkit().beep();
          evt.consume();
        }
    }//GEN-LAST:event_tfIdEspecKeyTyped

    private void tfIdPacienteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfIdPacienteFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_tfIdPacienteFocusGained

    private void tfIdPacienteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfIdPacienteFocusLost
        atualizaNomePaciente();
    }//GEN-LAST:event_tfIdPacienteFocusLost

    private void tfIdEspecFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfIdEspecFocusLost
        atualizaDescEspecialidade();
    }//GEN-LAST:event_tfIdEspecFocusLost

    private void tfIdPacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfIdPacienteMouseClicked
        if (evt.getClickCount() == 2) {
            ViewCadastroPaciente paciente = new ViewCadastroPaciente(null, true);
            paciente.isBusca = true;
            paciente.setVisible(true);
            tfIdPaciente.setText(paciente.id_selecionado);
            atualizaNomePaciente();
        }
    }//GEN-LAST:event_tfIdPacienteMouseClicked

    private void tfIdEspecMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfIdEspecMouseClicked
        if (evt.getClickCount() == 2) {
            int idMedico = 0;
            try{
                idMedico = Integer.parseInt(tfIdMedico.getText());
            } catch (Exception ex){
                idMedico = 0;
            }
            ViewCadastroEspecialidade espec = new ViewCadastroEspecialidade(null, true, idMedico);
            espec.isBusca = true;
            espec.setVisible(true);
            tfIdEspec.setText(espec.id_selecionado);
            atualizaDescEspecialidade();
        }
    }//GEN-LAST:event_tfIdEspecMouseClicked


 /*   public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ViewCadastroAgenda dialog = new ViewCadastroAgenda(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnCadastrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cbHoraFinal;
    private javax.swing.JComboBox<String> cbHoraInicial;
    private javax.swing.JComboBox<String> cbMinutoFinal;
    private javax.swing.JComboBox<String> cbMinutoInicial;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDataFinal1;
    private javax.swing.JLabel lblDataInicial;
    private javax.swing.JLabel lblDesc;
    private javax.swing.JLabel lblEspec;
    private javax.swing.JLabel lblHoraFinal;
    private javax.swing.JLabel lblHoraInicial;
    private javax.swing.JLabel lblHoraInicial1;
    private javax.swing.JLabel lblHoraInicial2;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblIdMed;
    private javax.swing.JLabel lblIdMed1;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JTextArea taObs;
    private javax.swing.JFormattedTextField tfDataInicial;
    private javax.swing.JTextField tfDesc;
    private javax.swing.JTextField tfDescEspec;
    private javax.swing.JTextField tfId;
    private javax.swing.JTextField tfIdEspec;
    private javax.swing.JTextField tfIdMedico;
    private javax.swing.JTextField tfIdPaciente;
    private javax.swing.JTextField tfNomeMedico;
    private javax.swing.JTextField tfNomePaciente;
    // End of variables declaration//GEN-END:variables
}
